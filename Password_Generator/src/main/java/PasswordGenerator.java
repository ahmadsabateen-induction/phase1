import java.util.Random;

public class PasswordGenerator {
    private static final Random RANDOM = new Random();
    private static final char[] SYMBOLS = {'_', '$', '#', '%'};
    private static final String MATCH_4_CONSECUTIVE_NUMBERS_REGEX = ".*\\d{4}.*";
    private static final String MATCH_2_CONSECUTIVE_LETTERS_REGEX = ".*[a-zA-Z]{2}.*";
    private static final String MATCH_2_CONSECUTIVE_SYMBOLS_REGEX = ".*[_$#%]{2}.*";


    public String generatePassword() {
        StringBuilder password = new StringBuilder();
        addNumbersToPassword(password);
        addSymbolsToPassword(password);
        addUpperCaseLettersToPassword(password);
        return shuffle(password.toString());
    }

    private void addNumbersToPassword(StringBuilder password) {
        for (int numCount = 0; numCount < 4; numCount++) {
            password.append((char) (RANDOM.nextInt(10) + '0'));
        }
    }

    private void addUpperCaseLettersToPassword(StringBuilder password) {
        for (int letterCount = 0; letterCount < 2; letterCount++) {
            password.append((char) (RANDOM.nextInt(26) + 'A'));
        }
    }

    private void addSymbolsToPassword(StringBuilder password) {
        for (int symbolCount = 0; symbolCount < 2; symbolCount++) {
            password.append((SYMBOLS[RANDOM.nextInt(SYMBOLS.length)]));
        }
    }

    private String shuffle(String password) {
        char[] charArray = password.toCharArray();
        for (int currentIndex = charArray.length - 1; currentIndex > 0; currentIndex--) {
            randomSwap(currentIndex, charArray);
        }
        String shuffledString = new String(charArray);
        if (hasConsecutiveNumbers(shuffledString) ||
                hasConsecutiveLetters(shuffledString) ||
                hasConsecutiveSymbols(shuffledString)) {
            return shuffle(shuffledString);
        }
        return shuffledString;
    }

    private void randomSwap(int currentIndex, char[] charArray) {
        int randomIndex = RANDOM.nextInt(currentIndex + 1);
        char temp = charArray[currentIndex];
        charArray[currentIndex] = charArray[randomIndex];
        charArray[randomIndex] = temp;
    }

    private boolean hasConsecutiveNumbers(String s) {
        return s.matches(MATCH_4_CONSECUTIVE_NUMBERS_REGEX);
    }

    private boolean hasConsecutiveLetters(String s) {
        return s.matches(MATCH_2_CONSECUTIVE_LETTERS_REGEX);
    }

    private boolean hasConsecutiveSymbols(String s) {
        return s.matches(MATCH_2_CONSECUTIVE_SYMBOLS_REGEX);
    }

}