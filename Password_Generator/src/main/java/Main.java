import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        PasswordGenerator generator = new PasswordGenerator();
        String password = generator.generatePassword();
        System.out.println(password);
        String s = "1_abcd2$";
        boolean hasFourConsecutiveLetters = s.matches("([a-zA-Z]){4}");
        System.out.println(hasFourConsecutiveLetters);
        String letters = "([a-zA-Z]){2,}";
        String numbers = "([0-9]){4,}";
        String symbol = "([_$#%]){2,}";

        Pattern pattern = Pattern.compile(letters);
        Matcher matcher = pattern.matcher(password);

        if (matcher.find()) {
            System.out.println("The string contains 4 consecutive letters.");
        } else {
            System.out.println("The string does not contain 4 consecutive letters.");
        }



    }
}
