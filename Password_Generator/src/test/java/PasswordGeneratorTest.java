import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

public class PasswordGeneratorTest {
    private final HashSet<Character> symbols = new HashSet<>(Arrays.asList('_', '$', '#', '%'));
    PasswordGenerator generator;
    @BeforeEach
    public void setUp(){
        generator = new PasswordGenerator();
    }
    @Test
    public void testGeneratePassword() {
        String password = generator.generatePassword();

        int numberCount = 0;
        int upperCaseCount = 0;
        int symbolCount = 0;
        for (char c : password.toCharArray()) {
            if (Character.isDigit(c)) {
                numberCount++;
            }
            else if (Character.isUpperCase(c)) {
                upperCaseCount++;
            }
            else if (symbols.contains(c)) {
                symbolCount++;
            }
        }
        assertEquals(4, numberCount);
        assertEquals(2, upperCaseCount);
        assertEquals(2, symbolCount);
    }


    @Test
    public void testGeneratePasswordLength() {
        String password = generator.generatePassword();
        assertEquals(8, password.length());
    }

    @Test
    public void testGeneratePasswordUnique() {
        String password1 = generator.generatePassword();
        String password2 = generator.generatePassword();
        assertNotEquals(password1, password2);
    }

    @Test
    public void testGeneratePasswordNoExceptions() {
        boolean exceptionThrown = false;
        try {
            generator.generatePassword();
        } catch (Exception e) {
            exceptionThrown = true;
        }
        assertFalse(exceptionThrown);
    }

    @Test
    public void testPasswordIsShuffled() {
        String lettersRegEx = "([A-Z]){2,}";
        String numbersRegEx = "([0-9]){4,}";
        String symbolRegEx = "([_$#%]){2,}";
        String password = generator.generatePassword();
        Pattern lettePattern = Pattern.compile(lettersRegEx);
        Pattern numbersPattern = Pattern.compile(numbersRegEx);
        Pattern symbolPattern = Pattern.compile(symbolRegEx);

        boolean isShuffled = !lettePattern.matcher(password).find() && !numbersPattern.matcher(password).find() && !symbolPattern.matcher(password).find();
        System.out.println(password);
        assertTrue(isShuffled);
    }

}