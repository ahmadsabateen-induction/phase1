package v3;

public class BinaryTreeV3 {
    private class Node {
        int data;
        Node left;
        Node right;

        public Node(int data) {
            this.data = data;
        }
    }

    private Node root;

    public boolean accept(int value) {
        if (root == null) {
            root = new Node(value);
            return true;
        }
        return accept(root, value);
    }


    private boolean accept(Node current, int value) {
        if (current.data == value) {
            return false;
        }
        if (value < current.data) {
            if(current.left == null) {
                current.left = new Node(value);
                return true;
            }
            return accept(current.left, value);
        } else {
            if(current.right == null) {
                current.right = new Node(value);
                return true;
            }
            return accept(current.right, value);
        }
    }

    public int depth(int value) {
        return depth(root, value, 0);
    }

    private int depth(Node node, int value, int depth) {
        if (node == null) {
            return -1;
        }
        if (node.data == value) {
            return depth;
        }
        int leftDepth = depth(node.left, value, depth + 1);
        if (leftDepth != -1) {
            return leftDepth;
        }
        return depth(node.right, value, depth + 1);
    }

    public int treeDepth() {
        return treeDepth(root);
    }

    private int treeDepth(Node node) {
        if (node == null) {
            return 0;
        }
        int leftDepth = treeDepth(node.left);
        int rightDepth = treeDepth(node.right);
        return Math.max(leftDepth, rightDepth) + 1;
    }
}
