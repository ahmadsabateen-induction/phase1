package v1;

public class Node {
    int data;
    Node left;
    Node right;

    public Node(int data) {
        this.data = data;
    }

    public boolean accept(int value) {
        if (value == data) {
            return false;
        }
        if (value > data) {
            if (right == null) {
                right = new Node(value);
                return true;
            }
            return right.accept(value);
        }
        if (left == null) {
            left = new Node(value);
            return true;
        }
        return left.accept(value);
    }
}