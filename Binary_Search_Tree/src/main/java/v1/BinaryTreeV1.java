package v1;

public class BinaryTreeV1 {
    private Node root;

    public boolean accept(int value) {
        if (root == null) {
            root = new Node(value);
            return true;
        }
        return root.accept(value);
    }

    public int depth(int value) {
        return depth(root, value, 0);
    }

    public int treeDepth() {
        return treeDepth(root);
    }

    private int treeDepth(Node node) {
        if (node == null) {
            return 0;
        }
        int leftDepth = treeDepth(node.left);
        int rightDepth = treeDepth(node.right);
        return Math.max(leftDepth, rightDepth) + 1;
    }

    private int depth(Node node, int value, int depth) {
        if (node == null) {
            return -1;
        }
        if (node.data == value) {
            return depth;
        }
        int leftDepth = depth(node.left, value, depth + 1);
        if (leftDepth != -1) {
            return leftDepth;
        }
        return depth(node.right, value, depth + 1);
    }
}