package v2;

public class BinaryTreeV2 {
    private int data;
    private BinaryTreeV2 left;
    private BinaryTreeV2 right;

    public BinaryTreeV2(int data) {
        this.data = data;
    }

    public boolean accept(int value) {
        if (data == value) {
            return false;
        } else if (data > value) {
            if (left == null) {
                left = new BinaryTreeV2(value);
                return true;
            } else {
                return left.accept(value);
            }
        } else {
            if (right == null) {
                right = new BinaryTreeV2(value);
                return true;
            } else {
                return right.accept(value);
            }
        }
    }

    public int depth(int value) {
        return depth(this, value, 0);
    }

    private int depth(BinaryTreeV2 node, int value, int depth) {
        if (node == null) {
            return -1;
        }
        if (node.data == value) {
            return depth;
        }
        int leftDepth = depth(node.left, value, depth + 1);
        if (leftDepth != -1) {
            return leftDepth;
        }
        return depth(node.right, value, depth + 1);
    }

    public int treeDepth() {
        int leftDepth = left != null ? left.treeDepth() : 0;
        int rightDepth = right != null ? right.treeDepth() : 0;
        return Math.max(leftDepth, rightDepth) + 1;
    }
}
