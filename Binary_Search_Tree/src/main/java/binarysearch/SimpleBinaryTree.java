package binarysearch;

public class SimpleBinaryTree implements BinaryTree {
    private final int data;
    private SimpleBinaryTree left;
    private SimpleBinaryTree right;

    public SimpleBinaryTree(int data) {
        this.data = data;
    }

    @Override
    public boolean accept(int value) {
        if (value == data) {
            return false;
        }
        if (value > data) {
            if (right == null) {
                right = new SimpleBinaryTree(value);
                return true;
            }
            return right.accept(value);
        }
        if (left == null) {
            left = new SimpleBinaryTree(value);
            return true;
        }
        return left.accept(value);
    }

    @Override
    public int depth(int value) {
        return 0;
    }

    @Override
    public int depth() {
        return 0;
    }

    void accept(SimpleBinaryTree simpleBinaryTree) {
        this.accept(simpleBinaryTree.data);
    }
}
