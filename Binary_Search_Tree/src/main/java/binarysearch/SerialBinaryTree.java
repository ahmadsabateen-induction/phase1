package binarysearch;

public class SerialBinaryTree implements BinaryTree {

    private final SimpleBinaryTree root;

    public SerialBinaryTree(SimpleBinaryTree root, SimpleBinaryTree... simpleBinaryTrees) {
        this.root = root;
        initBinarySearch(simpleBinaryTrees);
    }

    public SerialBinaryTree(int rootValue, int... values) {
        this.root = new SimpleBinaryTree(rootValue);
        initBinarySearch(values);
    }

    private void initBinarySearch(int[] values) {
        for (int value : values) {
            root.accept(value);
        }
    }

    @Override
    public boolean accept(int value) {
        return root.accept(value);
    }

    @Override
    public int depth(int value) {
        return root.depth(value);
    }

    @Override
    public int depth() {
        return root.depth();
    }

    private void initBinarySearch(SimpleBinaryTree[] simpleBinaryTrees) {
        for (SimpleBinaryTree node : simpleBinaryTrees) {
            root.accept(node);
        }
    }
}
