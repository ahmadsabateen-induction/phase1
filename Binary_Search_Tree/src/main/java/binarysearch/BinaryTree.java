package binarysearch;

public interface BinaryTree {
    boolean accept(int value);

    int depth(int value);

    int depth();
}
