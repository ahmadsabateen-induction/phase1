import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import v2.BinaryTreeV2;

class BinaryTreeV2Test {

    @Test
    void testAccept() {
        BinaryTreeV2 tree = new BinaryTreeV2(5);
        assertFalse(tree.accept(5));
        assertTrue(tree.accept(3));
        assertTrue(tree.accept(8));
        assertTrue(tree.accept(1));
        assertTrue(tree.accept(4));
        assertFalse(tree.accept(4));
    }

    @Test
    void testDepth() {
        BinaryTreeV2 tree = new BinaryTreeV2(8);
        tree.accept(8);
        assertEquals(0, tree.depth(8));
        tree.accept(3);
        tree.accept(10);
        assertEquals(1, tree.depth(3));
        tree.accept(1);
        assertEquals(2, tree.depth(1));
        tree.accept(6);
        assertEquals(2, tree.depth(6));
        assertEquals(-1, tree.depth(9));
    }

    @Test
    void testTreeDepth() {
        BinaryTreeV2 tree = new BinaryTreeV2(5);
        assertEquals(1, tree.treeDepth());
        tree.accept(3);
        tree.accept(8);
        assertEquals(2, tree.treeDepth());
        tree.accept(1);
        assertEquals(3, tree.treeDepth());
        tree.accept(4);
        assertEquals(3, tree.treeDepth());
    }

}
