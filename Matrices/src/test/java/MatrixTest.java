import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MatrixTest {
    @Test
    public void testExceptionWhenMatrixIsNotValid() {
        double[][] matrixData = {
                new double[]{-1d, -2d, 3d},
                new double[]{0d, 1d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d}
        };
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Matrix(matrixData);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("all columns in matrix must have equal length", thrown.getMessage());
    }

    @Test
    public void testAdditionWhenMatricesDimensionsAreNotEquals() {
        double[][] firstMatrix = {
                new double[]{-1d, -2d, 3d},
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };

        double[][] secondMatrix = {
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };
        Matrix leftMatrix = new Matrix(firstMatrix);
        Matrix rightMatrix = new Matrix(secondMatrix);
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            leftMatrix.add(rightMatrix);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("matrices should have equal dimensions", thrown.getMessage());
    }

    @Test
    public void testMultiplicationWhenMatricesDimensionsNotMatched() {
        double[][] firstMatrix = {
                new double[]{-1d, -2d},
                new double[]{0d, 1d},
                new double[]{3d, -1d},
                new double[]{2d, 1d}
        };

        double[][] secondMatrix = {
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };
        Matrix leftMatrix = new Matrix(firstMatrix);
        Matrix rightMatrix = new Matrix(secondMatrix);
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            leftMatrix.multiply(rightMatrix);
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("the number of columns of the left matrix should be the same as the number of rows of the right matrix", thrown.getMessage());
    }

    @Test
    public void testToUpperWhenMatrixNotSquare() {
        double[][] firstMatrix = {
                new double[]{-1d, -2d, 3d},
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };
        Matrix leftMatrix = new Matrix(firstMatrix);
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            leftMatrix.toUpper();
        }, "IllegalArgumentException was expected");
        Assertions.assertEquals("matrix is not square (NxN)", thrown.getMessage());
    }

    @Test
    public void testAddition() {
        double[][] firstMatrix = {
                new double[]{-1d, -2d, 3d},
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };

        double[][] secondMatrix = {
                new double[]{-1d, -2d, 3d},
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };
        double[][] expected = {
                new double[]{-2d, -4d, 6d},
                new double[]{0d, 2d, 8d},
                new double[]{6d, -2d, 8d},
                new double[]{4d, 2d, 0d}
        };
        Matrix leftMatrix = new Matrix(firstMatrix);
        Matrix rightMatrix = new Matrix(secondMatrix);
        Matrix expictedMatrix = new Matrix(expected);
        Assertions.assertEquals(expictedMatrix,
                leftMatrix.add(rightMatrix),
                "addition not working");
    }

    @Test
    public void testMultiplication() {
        double[][] firstMatrix = {
                new double[]{1d, 2d, 3d},
                new double[]{4d, 5d, 6d},
        };

        double[][] secondMatrix = {
                new double[]{7d, 8d},
                new double[]{9d, 10d},
                new double[]{11d, 12d}
        };
        double[][] expected = {
                new double[]{58d, 64d},
                new double[]{139d, 154d},
        };
        Matrix leftMatrix = new Matrix(firstMatrix);
        Matrix rightMatrix = new Matrix(secondMatrix);
        Matrix expictedMatrix = new Matrix(expected);
        Assertions.assertEquals(expictedMatrix,
                leftMatrix.multiply(rightMatrix),
                "multiplication not working");
    }

    @Test
    public void testToUpper() {
        double[][] input = {
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };


        double[][] expected = {
                new double[]{0d, 1d, 4d},
                new double[]{0d, -1d, 4d},
                new double[]{0d, 0d, 0d}
        };
        Matrix inputMatrix = new Matrix(input);
        Matrix expictedMatrix = new Matrix(expected);
        Assertions.assertEquals(expictedMatrix,
                inputMatrix.toUpper(),
                "toUpper not working");
    }

    @Test
    public void testMultiplyWithScalar() {
        double[][] input = {
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };
        double[][] expected = {
                new double[]{0d, 2d, 8d},
                new double[]{6d, -2d, 8d},
                new double[]{4d, 2d, 0d}
        };
        Matrix inputMatrix = new Matrix(input);
        Matrix expictedMatrix = new Matrix(expected);
        Assertions.assertEquals(expictedMatrix,
                inputMatrix.multiplyByScalar(2),
                "multiplyByScalar not working");
    }

    @Test
    public void testToLower() {
        double[][] input = {
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };


        double[][] expected = {
                new double[]{0d, 0d, 0d},
                new double[]{3d, -1d, 0d},
                new double[]{2d, 1d, 0d}
        };
        Matrix inputMatrix = new Matrix(input);
        Matrix expictedMatrix = new Matrix(expected);
        Assertions.assertEquals(expictedMatrix,
                inputMatrix.toLower(),
                "toLower not working");
    }

    @Test
    public void testToDiagonal() {
        double[][] input = {
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };


        double[][] expected = {
                new double[]{0d, 0d, 0d},
                new double[]{0d, -1d, 0d},
                new double[]{0d, 0d, 0d}
        };
        Matrix inputMatrix = new Matrix(input);
        Matrix expictedMatrix = new Matrix(expected);
        Assertions.assertEquals(expictedMatrix,
                inputMatrix.toDiagonal(),
                "toDiagonal not working");
    }

    @Test
    public void testTranspose() {
        double[][] input = {
                new double[]{0d, 1d},
                new double[]{3d, -1d},
                new double[]{2d, 1d}
        };


        double[][] expected = {
                new double[]{0d, 3d, 2d},
                new double[]{1d, -1d, 1d}
        };
        Matrix inputMatrix = new Matrix(input);
        Matrix expectedMatrix = new Matrix(expected);

        Assertions.assertEquals(expectedMatrix,
                inputMatrix.transpose(),
                "transpose not working");
    }

    @Test
    public void testSubMatrix() {
        double[][] input = {
                new double[]{0d, 1d, 4d},
                new double[]{3d, -1d, 4d},
                new double[]{2d, 1d, 0d}
        };


        double[][] expected = {
                new double[]{3d, -1d},
                new double[]{2d, 1d}
        };
        Matrix inputMatrix = new Matrix(input);
        Matrix expectedMatrix = new Matrix(expected);
        Assertions.assertEquals(expectedMatrix,
                inputMatrix.subMatrix(0, 2),
                "subMatrix not working");
    }

    @Test
    public void testMatrixDeterminant() {
        double[][] inputMatrix = {
                new double[]{-1d, -2d, 3d, 2d},
                new double[]{0d, 1d, 4d, -2d},
                new double[]{3d, -1d, 4d, 0d},
                new double[]{2d, 1d, 0d, 3d}
        };
        Matrix matrix = new Matrix(inputMatrix);
        Assertions.assertEquals(-185.0, matrix.matrixDeterminant());
    }
}
