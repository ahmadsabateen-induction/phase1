import java.util.Arrays;

public class Matrix {

    private final double[][] matrixData;
    private final int rowsNumber;
    private final int colNumber;
    private final boolean isSquare;

    public Matrix(double[][] matrixData) {
        validateMatrix(matrixData);
        this.matrixData = matrixData;
        rowsNumber = matrixData.length;
        colNumber = matrixData[0].length;
        isSquare = isMatrixSquare();
    }

    public Matrix add(Matrix other) {
        checkMatrixDimensionsForAddition(other);
        double[][] result = new double[rowsNumber][colNumber];
        for (int row = 0; row < rowsNumber; row++) {
            for (int col = 0; col < colNumber; col++) {
                result[row][col] = getCell(row, col) + other.getCell(row, col);
            }
        }
        return new Matrix(result);
    }

    public Matrix multiplyByScalar(double scalar) {
        double[][] result = new double[colNumber][rowsNumber];
        for (int row = 0; row < rowsNumber; row++) {
            for (int col = 0; col < colNumber; col++) {
                result[row][col] = getCell(row, col) * scalar;
            }
        }
        return new Matrix(result);
    }

    public Matrix transpose() {
        double[][] result = new double[colNumber][rowsNumber];
        for (int row = 0; row < rowsNumber; row++) {
            for (int col = 0; col < colNumber; col++) {
                result[col][row] = getCell(row, col);
            }
        }
        return new Matrix(result);
    }

    public Matrix multiply(Matrix other) {
        checkMatrixDimensionsForMultiplication(other);
        double[][] result = new double[rowsNumber][other.colNumber];

        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[0].length; col++) {
                result[row][col] = multiplyMatricesCell(matrixData, other.matrixData, row, col);
            }
        }

        return new Matrix(result);
    }

    public Matrix subMatrix(int deleteRow, int deleteCol) {
        int resultRows = deleteRow > -1 ? rowsNumber - 1 : rowsNumber;
        int resultCols = deleteCol > -1 ? colNumber - 1 : colNumber;
        double[][] result = new double[resultRows][resultCols];
        int newRow = 0;
        for (int row = 0; row < rowsNumber; ++row) {
            if (row != deleteRow) {
                int newCol = 0;
                for (int col = 0; col < colNumber; ++col) {
                    if (col != deleteCol) {
                        result[newRow][newCol] = getCell(row, col);
                        ++newCol;
                    }
                }
                ++newRow;
            }
        }
        return new Matrix(result);
    }

    public Matrix toUpper() {
        validateSquare();
        double[][] result = new double[rowsNumber][colNumber];
        for (int row = 0; row < rowsNumber; row++) {
            for (int col = row; col < colNumber; col++) {
                result[row][col] = getCell(row, col);
            }
        }
        return new Matrix(result);
    }

    public Matrix toLower() {
        validateSquare();
        double[][] result = new double[rowsNumber][colNumber];
        for (int row = 0; row < rowsNumber; row++) {
            for (int col = 0; col <= row; col++) {
                result[row][col] = getCell(row, col);
            }
        }
        return new Matrix(result);
    }

    public Matrix toDiagonal() {
        validateSquare();
        double[][] result = new double[rowsNumber][colNumber];
        for (int row = 0; row < rowsNumber; row++) {
            for (int col = 0; col <= row; col++) {
                if (row == col) result[row][col] = getCell(row, col);
            }
        }
        return new Matrix(result);
    }

    public double matrixDeterminant() {
        return calculateDeterminant(matrixData);
    }

    private double calculateDeterminant(double[][] matrix) {
        validateSquare();
        double temporary[][];
        double result = 0;

        if (matrix.length == 1) {
            result = matrix[0][0];
            return (result);
        }

        if (matrix.length == 2) {
            result = ((matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]));
            return (result);
        }

        for (int i = 0; i < matrix[0].length; i++) {
            temporary = new double[matrix.length - 1][matrix[0].length - 1];

            for (int j = 1; j < matrix.length; j++) {
                for (int k = 0; k < matrix[0].length; k++) {
                    if (k < i) {
                        temporary[j - 1][k] = matrix[j][k];
                    } else if (k > i) {
                        temporary[j - 1][k - 1] = matrix[j][k];
                    }
                }
            }

            result += matrix[0][i] * Math.pow(-1, i) * calculateDeterminant(temporary);
        }
        return (result);
    }

    private double multiplyMatricesCell(double[][] firstMatrix, double[][] secondMatrix, int row, int col) {
        double cell = 0;
        for (int i = 0; i < secondMatrix.length; i++) {
            cell += firstMatrix[row][i] * secondMatrix[i][col];
        }
        return cell;
    }

    private void validateSquare() {
        if (!isSquare) throw new IllegalArgumentException("matrix is not square (NxN)");
    }

    private void checkMatrixDimensionsForAddition(Matrix other) {
        if (!isEqualDimensions(other)) throw new IllegalArgumentException("matrices should have equal dimensions");

    }

    private void checkMatrixDimensionsForMultiplication(Matrix other) {
        if (colNumber != other.rowsNumber)
            throw new IllegalArgumentException("the number of columns of the left matrix should be the same as the number of rows of the right matrix");
    }

    private boolean isMatrixSquare() {
        return matrixData.length == matrixData[0].length;
    }

    private boolean isEqualDimensions(Matrix other) {
        return other.rowsNumber == rowsNumber && other.colNumber == colNumber;
    }

    private void validateMatrix(double[][] matrix) {
        int numOfRows = matrix.length;
        int numOfCol = matrix[0].length;
        for (int row = 0; row < numOfRows; row++) {
            if (matrix[row].length != numOfCol)
                throw new IllegalArgumentException("all columns in matrix must have equal length");
        }
    }

    private double getCell(int row, int col) {
        return matrixData[row][col];
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj.getClass() != this.getClass()) return false;
        Matrix other = (Matrix) obj;
        if (!isEqualDimensions(other)) return false;
        for (int row = 0; row < rowsNumber; row++) {
            for (int col = 0; col < colNumber; col++) {
                if (getCell(row, col) != other.getCell(row, col)) return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(matrixData);
    }
}
