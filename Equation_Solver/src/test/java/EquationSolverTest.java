import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EquationSolverTest {
    private EquationSolverV1 solver;
    @BeforeEach
    public void setUp() {
        solver = new EquationSolverV1(new EquationInputMock());
    }
    @Test
    void givenEquationWhenValidEquationThenReturnResultAsExpected() {
        String equation = "2^x+x*4/(y-1)";
        double actualResult = solver.evaluateEquation(equation);
        assertEquals(6, actualResult, 0.0);
    }

    @Test
    public void testEquationVariablesNotMatch() {
        String equation = "15*v";
        assertThrows(IllegalStateException.class, () -> solver.evaluateEquation(equation));
    }
    @Test
    public void testInvalidEquation() {
        String invalidEquation = "2+3*4/2-1)";
        assertThrows(ArithmeticException.class, () -> solver.evaluateEquation(invalidEquation));
    }
    private static class EquationInputMock implements EquationInput {
        @Override
        public HashMap<String, Double> equationInput(HashSet<String> variablesNames) {
            HashMap<String,Double> variablesNamesAndValues = new HashMap<>();
            variablesNamesAndValues.put("x",1.0);
            variablesNamesAndValues.put("y",2.0);
            return variablesNamesAndValues;
        }
    }
}
