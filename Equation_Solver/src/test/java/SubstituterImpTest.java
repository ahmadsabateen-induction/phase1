import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class SubstituterImpTest {
    @Test
    public void testSubstitute() {
        Substitute substituter = new Substitute(new EquationInputMock());
        String equation = "x + y";
        String expected = "2.0 + 3.0";
        String result = substituter.substitute(equation);
        assertEquals(expected, result);
    }
    private static class EquationInputMock implements EquationInput {
        @Override
        public HashMap<String, Double> equationInput(HashSet<String> variablesNames) {
            HashMap<String,Double> variablesNamesAndValues = new HashMap<>();
            variablesNamesAndValues.put("x",2.0);
            variablesNamesAndValues.put("y",3.0);
            return variablesNamesAndValues;
        }
    }
}
