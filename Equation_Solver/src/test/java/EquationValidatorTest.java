import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class EquationValidatorTest {
    private final EquationValidator validator = new EquationValidator();

    @Test
    public void testValidEquation() {
        String validEquation = "2+3*(4/2)-1";
        validator.validate(validEquation);
    }

    @Test
    public void testInvalidParentheses() {
        String invalidEquation = "2+3*4/2-1)";
        assertThrows(ArithmeticException.class, () -> validator.validate(invalidEquation));
    }

    @Test
    public void testInvalidOperatorsUsage() {
        String invalidEquation = "2+*3*4/2-1";
        assertThrows(ArithmeticException.class, () -> validator.validate(invalidEquation));
    }
}
