import java.util.Stack;

 class EquationValidator {
    public void validate(String equation) {
        if (!isValidParentheses(equation) || !isValidOperatorsUsage(equation))
            throw new ArithmeticException("Not valid equation");
    }

    private boolean isValidParentheses(String equation) {
        Stack<Character> parentheses = new Stack<>();
        for (int i = 0; i < equation.length(); i++) {
            char c = equation.charAt(i);
            if (c == '(') {
                parentheses.push(c);
            } else if (c == ')') {
                if (parentheses.isEmpty() || parentheses.peek() != '(') {
                    return false;
                } else {
                    parentheses.pop();
                }
            }
        }

        return parentheses.isEmpty();
    }

    private boolean isValidOperatorsUsage(String equation) {
        if (Utils.isOperator(equation.charAt(0)))
            return false;
        if (Utils.isOperator(equation.charAt(equation.length() - 1)))
            return false;
        for (int index = 1; index < equation.length() - 1; index++) {
            char charAtIndex = equation.charAt(index);
            if (Utils.isOperator(charAtIndex)) {
                if (!Character.isDigit(equation.charAt(index - 1)) && !Character.isAlphabetic(equation.charAt(index - 1)) && equation.charAt(index - 1) != ')')
                    return false;
                if (!Character.isDigit(equation.charAt(index + 1)) && !Character.isAlphabetic(equation.charAt(index + 1)) && equation.charAt(index + 1) != '(')
                    return false;
            }
        }
        return true;
    }

}
