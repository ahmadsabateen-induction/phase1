import java.util.HashMap;
import java.util.HashSet;

public interface EquationInput {
        HashMap<String,Double> equationInput(HashSet<String> variablesNames);
}
