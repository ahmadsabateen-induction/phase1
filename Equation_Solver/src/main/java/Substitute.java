import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Substitute {
    private final EquationInput equationInput;

     Substitute(EquationInput equationInput) {
        this.equationInput = equationInput;
    }

    public String substitute(String equation) {
        String regex = "[a-zA-Z]";
        Matcher matcher = Pattern.compile(regex).matcher(equation);
        HashSet<String> variablesNames = new HashSet<>();

        while (matcher.find()) {
            String variable = matcher.group();
            variablesNames.add(variable);
        }
        Map<String, Double> variablesAndValues = equationInput.equationInput(variablesNames);
        for (Map.Entry<String, Double> entry : variablesAndValues.entrySet()) {
            equation = equation.replace(entry.getKey(), Double.toString(entry.getValue()));
        }
        for (String variable: variablesAndValues.keySet()) {
            if (!variablesNames.contains(variable)){
                throw new IllegalStateException("variable: "+variable+" is not in the equation");
            }
        }
        return equation;
    }
}
