import java.util.Stack;

public class EquationSolverV1 {

    private final EquationValidator validator = new EquationValidator();
    private final Substitute modifier;

    public EquationSolverV1(EquationInput equationInput) {
            modifier = new Substitute(equationInput);
    }

    public double evaluateEquation(String equation) {
        validator.validate(equation);
        equation = modifier.substitute(equation);
        return solve(equation);
    }
    private double solve(String equation) {
        Stack<Double> operands = new Stack<>();
        Stack<Character> operators = new Stack<>();

        for (int index = 0; index < equation.length(); index++) {
            char currentSymbol = equation.charAt(index);

            if (Character.isDigit(currentSymbol)) {
                index = handleOperand(operands, equation, index);
            } else if (currentSymbol == '(') {
                handleOpenParenthesis(operators, currentSymbol);
            } else if (currentSymbol == ')') {
                handleClosedParenthesis(operands, operators);
            } else if (Utils.isOperator(currentSymbol)) {
                handleOperator(operands, operators, currentSymbol);
            }
        }

        handleRemainingOperations(operands, operators);
        return operands.pop();
    }

    private int handleOperand(Stack<Double> operands, String equation, int index) {
        operands.push(parseOperand(equation, index));
        return index + getLengthOfOperand(equation, index) - 1;
    }

    private void handleOpenParenthesis(Stack<Character> operators, char currentSymbol) {
        operators.push(currentSymbol);
    }

    private void handleClosedParenthesis(Stack<Double> operands, Stack<Character> operators) {
        while (operators.peek() != '(') {
            double operand2 = operands.pop();
            double operand1 = operands.pop();
            char operator = operators.pop();
            operands.push(evaluate(operand1, operand2, operator));
        }
        operators.pop();
    }

    private void handleOperator(Stack<Double> operands, Stack<Character> operators, char currentSymbol) {
        while (!operators.isEmpty() && precedence(operators.peek()) > precedence(currentSymbol)) {
            double operand2 = operands.pop();
            double operand1 = operands.pop();
            char operator = operators.pop();
            operands.push(evaluate(operand1, operand2, operator));
        }
        operators.push(currentSymbol);
    }

    private void handleRemainingOperations(Stack<Double> operands, Stack<Character> operators) {
        while (!operators.isEmpty()) {
            double operand2 = operands.pop();
            double operand1 = operands.pop();
            char operator = operators.pop();
            operands.push(evaluate(operand1, operand2, operator));
        }
    }
    private double parseOperand(String equation, int startIndex) {
        int length = getLengthOfOperand(equation, startIndex);
        String operandString = equation.substring(startIndex, startIndex + length);
        return Double.parseDouble(operandString);
    }

    private int getLengthOfOperand(String equation, int startIndex) {
        int length = 0;
        while (startIndex + length < equation.length() &&
                (Character.isDigit(equation.charAt(startIndex + length)) ||
                        equation.charAt(startIndex + length) == '.')) {
            length++;
        }
        return length;
    }
    private int precedence(char c) {
        if (c == '+' || c == '-') {
            return 1;
        } else if (c == '*' || c == '/') {
            return 2;
        } else if (c == '^') {
            return 3;
        } else {
            return 0;
        }
    }

    private double evaluate(double operand1, double operand2, char operator) {
        switch (operator) {
            case '+':
                return operand1 + operand2;
            case '-':
                return operand1 - operand2;
            case '*':
                return operand1 * operand2;
            case '/':
                return operand1 / operand2;
            case '^':
                return Math.pow(operand1, operand2);
            default:
                throw new IllegalArgumentException("Invalid operator: " + operator);
        }
    }

}
