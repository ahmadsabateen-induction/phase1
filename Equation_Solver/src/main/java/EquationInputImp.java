import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class EquationInputImp implements EquationInput{
    private static final Scanner scanner = new Scanner(System.in);
    @Override
    public HashMap<String,Double> equationInput(HashSet<String> variablesNames) {
        HashMap<String,Double> variablesNamesAndValues = new HashMap<>();
        for (String variable: variablesNames) {
        System.out.print("Enter the value of " + variable + ": ");
            double value = scanner.nextDouble();
            variablesNamesAndValues.put(variable, value);
        }
        return variablesNamesAndValues;
    }
}
