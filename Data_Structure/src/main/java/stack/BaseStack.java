package stack;

import stack.exceptions.EmptyStackException;
import java.util.Iterator;

abstract class BaseStack<E> implements Iterable<E>{
    protected E[] elements;
    protected int currentSize;
    protected static final int DEFAULT_INITIAL_CAPACITY = 16;

    @SuppressWarnings("unchecked")
    public BaseStack() {
        elements = (E[]) new Object[DEFAULT_INITIAL_CAPACITY];
    }
    @SuppressWarnings("unchecked")
    public BaseStack(int capacity) {
        elements = (E[]) new Object[capacity];
    }

    public E pop() {
        if (currentSize == 0) {
            throw new EmptyStackException();
        }
        E item = elements[--currentSize];
        elements[currentSize] = null;
        return item;
    }

    public E peek() {
        if (currentSize == 0) {
            throw new EmptyStackException();
        }
        return elements[currentSize - 1];
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

    public int size() {
        return currentSize;
    }
    public void printStackData() {
        for (E element : elements) {
        }
        System.out.println();
    }
    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            int index = 0;
            @Override
            public boolean hasNext() {
                return  index < size() && elements[index] != null;
            }
            @Override
            public E next() {
                if (hasNext() && elements[index] != null) {
                    return elements[index++];
                } else {
                    throw new IllegalStateException("No more elements in the array");
                }
            }
        };
    }
    abstract public void push(E item);
}
