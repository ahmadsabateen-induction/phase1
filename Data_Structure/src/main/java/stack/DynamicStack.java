package stack;
public class DynamicStack<E> extends BaseStack<E> {
    public DynamicStack(int capacity){
        super(capacity);
    }
    public DynamicStack(){
        super();
    }
    public void push(E item) {
        ensureCapacity();
        elements[currentSize++] = item;
    }

    @SuppressWarnings("unchecked")
    private void ensureCapacity() {
        if (elements.length == currentSize) {
            E[] oldElements = elements;
            elements = (E[]) new Object[2 * currentSize + 1];
            System.arraycopy(oldElements, 0, elements, 0, currentSize);
        }
    }
}