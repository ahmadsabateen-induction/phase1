package stack;

import stack.exceptions.EmptyStackException;
import stack.exceptions.FullStackException;

public class FixedSizeStack<E> extends BaseStack<E> {
    private static final int DEFAULT_INITIAL_CAPACITY = 50;

    public FixedSizeStack(int capacity) {
        super(capacity);
    }
    public FixedSizeStack() {
        super(DEFAULT_INITIAL_CAPACITY);
    }
    public void push(E item) {
        if (currentSize == elements.length) {
            throw new FullStackException();
        }
        elements[currentSize++] = item;
    }
}
//TODO add defult contructor ,defult size ,add forEach on the stack data
