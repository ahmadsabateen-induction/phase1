package queue;
public class FixedSizeQueue<T> extends BaseQueue<T> {

    @SuppressWarnings("unchecked")
    public FixedSizeQueue(int capacity) {
        super(capacity);
    }

    public void enqueue(T item) {
        if (size() == elements.length - 1) {
            throw new IllegalStateException("Queue is full");
        }
        elements[tail] = item;
        tail = (tail + 1) % elements.length;
    }
}
