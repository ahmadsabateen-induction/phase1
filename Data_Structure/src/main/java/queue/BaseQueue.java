package queue;

import java.util.Iterator;
import java.util.NoSuchElementException;

abstract class BaseQueue<E> implements Iterable<E>{
    protected E[] elements;
    protected int head;
    protected int tail;
    protected static final int DEFAULT_INITIAL_CAPACITY = 16;

    @SuppressWarnings("unchecked")
    public BaseQueue() {
        elements = (E[]) new Object[DEFAULT_INITIAL_CAPACITY];
    }
    @SuppressWarnings("unchecked")
    public BaseQueue(int capacity) {
        elements = (E[]) new Object[capacity];
    }


    public E dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        E element = elements[head];
        elements[head] = null;
        head = (head + 1) % elements.length;
        return element;
    }

    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return elements[head];
    }
    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            int index = 0;
            @Override
            public boolean hasNext() {
                return  index < size() && elements[index] != null;
            }
            @Override
            public E next() {
                if (hasNext() && elements[index] != null) {
                    return elements[index++];
                } else {
                    throw new IllegalStateException("No more elements in the array");
                }
            }
        };
    }
    public int size() {
        return (tail - head + elements.length) % elements.length;
    }

    public boolean isEmpty() {
        return head == tail;
    }
    abstract public void enqueue(E element);
}
