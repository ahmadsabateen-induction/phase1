package queue;

import java.util.NoSuchElementException;

public class DynamicQueue<E> extends BaseQueue<E> {

    public DynamicQueue() {super();}
    public DynamicQueue(int capacity) {
        super(capacity);
    }


    public void enqueue(E element) {
        ensureCapacity();
        elements[tail] = element;
        tail = (tail + 1) % elements.length;
    }

    @SuppressWarnings("unchecked")
    private void ensureCapacity() {
        if (size() == elements.length - 1) {
            E[] oldElements = elements;
            elements = (E[]) new Object[2 * elements.length];
            System.arraycopy(oldElements, head, elements, 0, oldElements.length - head);
            System.arraycopy(oldElements, 0, elements, oldElements.length - head, tail);
            head = 0;
            tail = oldElements.length;
        }
    }
}
