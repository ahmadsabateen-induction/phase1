package stack;

import stack.exceptions.EmptyStackException;
import stack.exceptions.FullStackException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FixedSizeStackTest {
    private BaseStack<Integer> stack;

    @BeforeEach
    public void setUp() {
        stack = new FixedSizeStack<>(10);
    }

    @Test
    public void testPush() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        assertEquals(3, stack.size());
    }

    @Test
    public void testPop() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        assertEquals(3, stack.pop().intValue());
        assertEquals(2, stack.pop().intValue());
        assertEquals(1, stack.pop().intValue());
        assertTrue(stack.isEmpty());
    }

    @Test
    public void testPeek() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        assertEquals(3, stack.peek().intValue());
        assertEquals(3, stack.peek().intValue());
        assertEquals(3, stack.pop().intValue());
        assertEquals(2, stack.peek().intValue());
        assertEquals(2, stack.pop().intValue());
        assertEquals(1, stack.peek().intValue());
        assertEquals(1, stack.pop().intValue());
        assertTrue(stack.isEmpty());
    }

    @Test
    public void testPopOnEmptyStack() {
        assertThrows(EmptyStackException.class, () -> stack.pop());
    }

    @Test
    public void testPeekOnEmptyStack() {
        assertThrows(EmptyStackException.class, () -> stack.peek());
    }

    @Test
    public void testPushOnFullStack() {
        for (int i = 0; i < 10; i++) {
            stack.push(i);
        }
        assertThrows(FullStackException.class, () -> stack.push(11));
    }
    @Test
    public void test(){
        BaseStack<Integer> stack = new DynamicStack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        int sum = 0;
        for (Integer item : stack){
            sum+=item;
        }
        assertEquals(6,sum);
    }
}


