package queue;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FixedSizeQueueTest {
    private BaseQueue<Integer> queue = new FixedSizeQueue<>(5);

    @Test
    void testEnqueue() {
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        assertEquals(3, queue.size());
        assertFalse(queue.isEmpty());
    }

    @Test
    void testDequeue() {
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        assertEquals(1, queue.dequeue());
        assertEquals(2, queue.size());
        assertFalse(queue.isEmpty());
    }

    @Test
    void testPeek() {
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        assertEquals(1, queue.peek());
        assertEquals(1, queue.peek());
        assertEquals(3, queue.size());
        assertFalse(queue.isEmpty());
    }

    @Test
    void testIsEmpty() {
        assertTrue(queue.isEmpty());
        queue.enqueue(1);
        assertFalse(queue.isEmpty());
        queue.dequeue();
        assertTrue(queue.isEmpty());
    }
    @Test
    public void test() {
        BaseQueue<Integer> queue = new DynamicQueue<>();
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        int sum = 0;
        for (Integer item : queue) {
            sum += item;
        }
        assertEquals(6, sum);
    }
}
