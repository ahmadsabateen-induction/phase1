package queue;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class DynamicQueueTest {
    private BaseQueue<Integer> queue;

    @Test
    void testConstructor() {
        queue = new DynamicQueue<>();
        assertTrue(queue.isEmpty());
        assertEquals(0, queue.size());
    }

    @Test
    void testEnqueue() {
        queue = new DynamicQueue<>();
        queue.enqueue(1);
        assertFalse(queue.isEmpty());
        assertEquals(1, queue.size());
        queue.enqueue(2);
        assertEquals(2, queue.size());
        queue.enqueue(3);
        assertEquals(3, queue.size());
    }

    @Test
    void testDequeue() {
        queue = new DynamicQueue<>();
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        assertEquals(1, queue.dequeue());
        assertEquals(2, queue.size());
        assertEquals(2, queue.dequeue());
        assertEquals(1, queue.size());
        assertEquals(3, queue.dequeue());
        assertTrue(queue.isEmpty());
    }

    @Test
    void testPeek() {
        queue = new DynamicQueue<>();
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        assertEquals(1, queue.peek());
        assertEquals(3, queue.size());
        queue.dequeue();
        assertEquals(2, queue.peek());
        assertEquals(2, queue.size());
    }

    @Test
    public void testForeach() {
        BaseQueue<Integer> queue = new DynamicQueue<>();
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        int sum = 0;
        for (Integer item : queue) {
            sum += item;
        }
        assertEquals(6, sum);
    }
}
